angular.module('hostMeetups', ['satellizer'])

.config(['$authProvider', function($authProvider) {
    $authProvider.twitter({
      url: '/api/user/login'
    });
}]);
