/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Twit = require('twit');

module.exports = {
	tweet: function(req, res, next) {
    var T = new Twit({
      consumer_key:           sail.config.twitterAuth.TWITTER_KEY,
      consumer_secret:        sail.config.twitterAuth.TWITTER_SECRET

    })
    T.post('statuses/update', { status: 'hello world!' }, function(err, data, response) {
      console.log(data);
    })
  }

};

